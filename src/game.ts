/////////////////////////////////////
// Building in Decentraland course
// Lesson 9 scene to fix
// 
// this scene started out as a DCL 2.2.6, SDK 5.0.0 "dcl init" scene, as follows:
/*
npm rm -g decentraland
npm i -g decentraland@2.2.5
md Lesson9a-SDK5-FixMe
cd Lesson9a-SDK5-FixMe
dcl init
npm rm decentraland-ecs
npm i decentraland-ecs@5.0.0
// imported an SDK5 version of spawnerFunctions.ts module
// got busy reworking the game.ts file to have the basis for the Lesson assignment
*/

import {spawnPlaneX, spawnBoxX, spawnGltfX} from './modules/spawnerFunctions'

let scene = new Entity()
let sceneTransform = new Transform({
    position: new Vector3(16, 0, 16),
    rotation: Quaternion.Euler(0, 180, 0)
})
scene.addComponent(sceneTransform)
engine.addEntity(scene)

const ground = spawnBoxX(8,0,8,  90,0,0,  16,16,0.1)
const groundTexture = new Texture("materials/grass-free-texture_512x512.jpg")
const groundMaterial = new Material()
groundMaterial.albedoTexture = groundTexture
ground.addComponent(groundMaterial)
ground.setParent(scene)

const box = spawnBoxX(5,1,5, 0,0,0,  1,1,1)
const boxMaterial = new Material()
const colorGreenDark = Color3.FromHexString("#006400")
boxMaterial.albedoColor = colorGreenDark
boxMaterial.reflectivityColor = colorGreenDark
boxMaterial.emissiveIntensity = 1
boxMaterial.metallic = 1
boxMaterial.roughness = 0
box.addComponent(boxMaterial)
box.setParent(scene)

const cube = spawnGltfX(new GLTFShape("models/Lesson9Cube/Lesson9Cube.glb"), 5,1,6, 0,0,0, 0.5,0.5,0.5)
cube.setParent(scene)

const naturePack = spawnGltfX(new GLTFShape("models/NaturePack/Nature-Pack-gltf-v1.6.gltf"), 11,0,5, 0,0,0,  1,1,1)
naturePack.setParent(scene)

const firTree = spawnGltfX(new GLTFShape("models/TreeA_Fir/TreeA_Optimised_28_June_2018_A01.babylon.gltf"), 10,0,10, 0,0,0,  .15,.15,.15)
firTree.setParent(scene)

const bird = spawnGltfX(new GLTFShape("models/Sparrow/Sparrow-burung-pipit.gltf"), 5,1,2, 0,0,0, 0.05, 0.05, 0.05)
bird.addComponent(new Animator())
bird.getComponent(Animator).addClip(new AnimationState("fly"))
bird.getComponent(Animator).getClip("fly").play()
bird.setParent(scene)
